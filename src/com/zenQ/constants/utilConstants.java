package com.zenQ.constants;

public class utilConstants{
	
		
	static String currentDir = System.getProperty("user.dir");
	
	public static final String applicationURL = "http://www.newtours.demoaut.com//";
		
	public static final String pathDataSheet = currentDir+"/testData/DataSheet.xlsx";
	
	public static final String pathTestCases = currentDir+"/testData/testCases.xlsx";
	
	public static final String pathChromeDriver = currentDir+"/drivers/chromedriver.exe";
	
	public static final String pathgeckoDriver = currentDir+"/drivers/geckodriver.exe";
	
	public static final String pathScreenShotsFolder = currentDir + "/Screens";
	
	public static final String pathReports = currentDir +"/Reports";
	
	public static final String pathExtentConfigFile = currentDir+"/libs/extent-config.xml";
	
		

}
