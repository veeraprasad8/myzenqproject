package com.zenQ.driver;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

import org.openqa.selenium.WebDriver;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.zenQ.constants.utilConstants;

import functionLibrary.Utilities;
import functionLibrary.Waits;

public class Driver {

	public static WebDriver driver;
	public static Utilities util;
	public static Waits waits;
	public static ExtentReports extentReport;
	public static ExtentTest extentTest;
	
	public static void main(String[] args) throws Exception {
				
		   extentReport = new ExtentReports(utilConstants.pathReports+"/Report "+ Utilities.timeStampGenerator()+".html",false);
		   extentTest = new ExtentTest("MercuryTours", "Test Automation");
		   extentReport.loadConfig(new File(utilConstants.pathExtentConfigFile));
		   extentTest = extentReport.startTest("Driver Class");
		   
		   System.out.println(" ***********Starting Tests************ ");
		   extentTest.log(LogStatus.INFO, "***************Initialize selenium***************");
		   extentTest.log(LogStatus.INFO, "******************Starting Tests*****************");
			
			String browser=null;	
			String execute = null;
			String cName=null, mName=null;
					
		/****************************************************************************************************************/
		
								browser = "chrome";
		
		/***************************************************************************************************************/
		
				Utilities ut = new Utilities();
											
				extentTest.log(LogStatus.INFO, "**********Tests Running Browser: "+browser+"*********");
				System.out.println("*********** Launching "+browser+" Browser ***********");
				extentReport.endTest(extentTest);										
				
				Utilities.setExcelFile(utilConstants.pathTestCases, 0);
				int rows = Utilities.getRowCount(0);
							
				for(int i=1;i<rows;i++){
					Utilities.setExcelFile(utilConstants.pathTestCases, 0);
					execute = Utilities.getCellData(i, 2);
										
					if(execute.equalsIgnoreCase("Y")){
						
						driver = Utilities.InitializeDriver(browser);
						ut.loadURL();
						
						String className = Utilities.getCellData(i, 0).trim();
						cName = "testScripts."+className;
						mName = Utilities.getCellData(i, 1).trim();
											
						extentTest = extentReport.startTest(className);  
						extentTest.log(LogStatus.INFO, "####################  Executing Test Case : "+className+" ####################");
			             
						System.out.println("*********** Executing TestCase : "+Utilities.getCellData(i, 0)+" ***********");
					
						 Class<?> thisClass = Class.forName(cName);
						 Constructor<?> constructor = thisClass.getConstructor(WebDriver.class);
						 Object iClass = (Object)constructor.newInstance(driver);
						 Method thisMethod = thisClass.getDeclaredMethod(mName);
						 Object obj = thisMethod.invoke(iClass);
						 String result = obj.toString();
						 
						 if(result.equalsIgnoreCase("Fail")){
			            	  String ssName = className+"_"+Utilities.timeStampGenerator();
			            	  ut.getScreenShot(ssName);
			            	  extentTest.log(LogStatus.FAIL, "*************** Test case failed for: "+className+" ***************");
			            	  extentTest.log(LogStatus.FAIL,  extentTest.addScreenCapture(utilConstants.pathScreenShotsFolder+"/"+ssName+".jpg"));            	 
			              }else{
			            	  extentTest.log(LogStatus.PASS, "*************** Test case passed for: "+className+" ***************");
			              }
			              extentReport.endTest(extentTest);
			              extentReport.flush();
						 
						 Utilities.setExcelFile(utilConstants.pathTestCases, 0);
						 ut.putCellData(utilConstants.pathTestCases, 0, 1, 3, result);
						 driver.close();
						 driver.quit();
						
					}else continue;						
					
					
				} 	
				
	}

}
