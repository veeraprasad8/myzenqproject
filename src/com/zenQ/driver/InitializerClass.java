package com.zenQ.driver;



import functionLibrary.Utilities;
import functionLibrary.Waits;
import pageObjects.FlightBooking_POM;
import pageObjects.Login_POM;
import pageObjects.Register_POM;

public abstract class InitializerClass extends Driver{
	
	protected InitializerClass(){	
		util = new Utilities();
		waits = new Waits();
	}

	
	//Objects for POM classes
	public Register_POM register;
	public Login_POM login;
	public FlightBooking_POM flight;

	
}
