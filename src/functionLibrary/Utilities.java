package functionLibrary;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.commons.io.FileUtils;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import com.zenQ.constants.utilConstants;
import com.zenQ.driver.Driver;

import pageObjects.Login_POM;

public class Utilities extends Driver{
	
	 public Utilities(){
	    	
	  }
	
	private static XSSFSheet ExcelWSheet;
    private static XSSFWorkbook ExcelWBook;
    private static XSSFCell Cell;
		
	/*Initialize WebDriver*/
	
	public static WebDriver InitializeDriver(String browser) throws Exception{
		taskKill();
		WebDriver driver;
		switch (browser) {
		
		case "chrome":
			System.setProperty("webdriver.chrome.driver",utilConstants.pathChromeDriver);
			driver = new ChromeDriver();
			break;
		
		case "firefox":
			System.setProperty("webdriver.gecko.driver", utilConstants.pathgeckoDriver);
			driver = new FirefoxDriver();
			break;
			
		default:
			System.setProperty("webdriver.gecko.driver",utilConstants.pathgeckoDriver);
			driver = new FirefoxDriver();
			break;
		}
				
		return driver;
	}
	
	
	public void loadURL() throws Exception{
		driver.manage().window().maximize();
		driver.get(utilConstants.applicationURL);
	}
	
	
	public static void loginApplication(){
		Login_POM login_POM = new Login_POM();
		login_POM.loginToApp();	
	}
	
	
	 public static void setExcelFile(String Path,int sheetNum) throws Exception {
	        
         FileInputStream ExcelFile = new FileInputStream(Path);
         ExcelWBook = new XSSFWorkbook(ExcelFile);
         ExcelWSheet = ExcelWBook.getSheetAt(sheetNum);
         
	 }
	
	
	public static String getCellData(int RowNum, int ColNum) throws Exception{
        
		 Cell = ExcelWSheet.getRow(RowNum).getCell(ColNum);
		 String CellData = Cell.getStringCellValue();
		 return CellData;

	}
	public void putCellData(String filePath,int sheetNo,int RowNum, int ColNum, String Value) throws Exception{
	    
		
	       ExcelWSheet = ExcelWBook.getSheetAt(sheetNo); 
	       Cell = ExcelWSheet.getRow(RowNum).getCell(ColNum);
	       Cell.setCellValue(Value);
	       FileOutputStream out = new FileOutputStream(new File(filePath));     
	       ExcelWBook.write(out);
	       out.flush();
		   out.close();
	  
    }
	
	
	public static int getRowCount(int sheet){
        ExcelWSheet = ExcelWBook.getSheetAt(sheet);  
        int number=ExcelWSheet.getLastRowNum()+1;
        return number;
    }
	
	public String getEmailId() throws Exception{
		
		DateFormat  dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		Date date = new Date();
		dateFormat.format(date);
		String emailID = "test"+dateFormat.format(date)+"@gmail.com";
		
		return emailID;
		
	}
	
	public static void taskKill() throws Exception{
		Runtime rt = Runtime.getRuntime();
		rt.exec("taskkill /F /IM chromedriver.exe /T");
		Thread.sleep(1000);
	}
	
	public static String timeStampGenerator()	{
		
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-hhmmss");
		Date date1 = new Date();
		String timestamp = dateFormat.format(date1);
		return timestamp;
		
	}	
	public void getScreenShot(String fileName) throws IOException{
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile, new File(utilConstants.pathScreenShotsFolder+"/"+fileName+".jpg"));
	}
	}

