package functionLibrary;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import com.zenQ.driver.InitializerClass;


public class actions extends InitializerClass{
	
	public actions(){
		
	}
			
	 public static void waitAndType(WebElement element, String value){
		 element.sendKeys(value);
	 }

	 
	 public static void waitAndClick(WebElement element){		   
		 waits.WaitForClickableElement(element);
		 element.click();
		    	   
	   }
	 
	 public static void selectValue(WebElement element, String value){
		 Select select = new Select(element);
		 select.selectByValue(value);
	 }
	 
	 public void waitAndDoubleClick(WebElement element){
		 Actions a = new Actions(driver);
		 waits.WaitForClickableElement(element);
		 a.doubleClick(element).build().perform();
		 
	 }
	 public static void javascriptClick(WebElement element) throws Exception{
			
		 Thread.sleep(1000);
		 JavascriptExecutor js = (JavascriptExecutor)driver;
		 js.executeScript("arguments[0].click();",element);
		
	}
		
	

}