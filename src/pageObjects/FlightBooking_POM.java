package pageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;

import com.zenQ.driver.Driver;

public class FlightBooking_POM extends Driver {

	
	public  FlightBooking_POM() {

		ElementLocatorFactory locatorWait = new AjaxElementLocatorFactory(driver, 30);
		PageFactory.initElements(locatorWait, this);
	
	}
	@FindBy(xpath="//input[@value='oneway']")
	private WebElement Type;
	public WebElement getType_RadioButton(){
		return Type;
	}
	
	@FindBy(xpath="//select[@name='passCount']")
	private WebElement Passengers;
	public WebElement getPassengers(){
		return Passengers;
	}
	

	@FindBy(xpath="//select[@name='fromPort']")
	private WebElement Departure;
	public WebElement getDeparture(){
		return Departure;
	}
	
	@FindBy(xpath="//input[@name='findFlights']")
	private WebElement Continue;
	public WebElement getContine_Button(){
		return Continue;
	}
	

	@FindBy(xpath="//input[@name='reserveFlights']")
	private WebElement Reserve_Continue;
	public WebElement getReserveContine_Button(){
		return Reserve_Continue;
	}
	

	@FindBy(xpath="//input[@name='buyFlights']")
	private WebElement Secure_Purchase;
	public WebElement getSecure_Purcharse(){
		return Secure_Purchase;
	}
	
	
}
