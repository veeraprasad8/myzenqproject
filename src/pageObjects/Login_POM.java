package pageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;

import com.zenQ.driver.Driver;

public class Login_POM extends Driver {

	public Login_POM(){
		ElementLocatorFactory locatorWait = new AjaxElementLocatorFactory(driver, 30);
		PageFactory.initElements(locatorWait, this);
	}
	
	@FindBy(name="userName")
	private WebElement User_Name;
	public WebElement getUser_Name(){
		return User_Name;
	}
	
	@FindBy(name="password")
	private WebElement Pass_Word;
	public WebElement getPassword(){
		return Pass_Word;
	}
	
	@FindBy(name="login")
	private WebElement Sign_In;
	public WebElement getSingIn_Button(){
		return Sign_In;
	}
	
	
	public void loginToApp(){
		
		User_Name.sendKeys("veera");
		Pass_Word.sendKeys("veera");
		Sign_In.click();
	}
}
