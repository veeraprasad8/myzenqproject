package pageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;

import com.zenQ.driver.Driver;

public class Register_POM extends Driver {
	
	
	public Register_POM(){
		ElementLocatorFactory locatorWait = new AjaxElementLocatorFactory(driver, 30);
		PageFactory.initElements(locatorWait, this);
	}
	
	@FindBy(linkText="REGISTER")
	private WebElement Lnk_Register;
	public WebElement getLnk_Register(){
		return Lnk_Register;
	}

	@FindBy(name="firstName")
	private WebElement firstName;
	public WebElement getFirstName() {
	   return firstName;
	}
	
	@FindBy(name="lastName")
	private WebElement lastName;
	public WebElement getLastName() {
		return lastName;
	}
	@FindBy(name="phone")
	private WebElement phoneNumber;
	public WebElement getphoneNumber(){
		return phoneNumber;
	}
	
	@FindBy(id="userName")
	private WebElement email;
	public WebElement getEmail() {
		return email;
	}
	@FindBy(name="address1")
	private WebElement Address1;
	public WebElement getAddress1() {
		return Address1;
	}
	@FindBy(name="city")
	private WebElement City;
	public WebElement getCity() {
		return City;
	}
	@FindBy(name="state")
	private WebElement State;
	public WebElement getState() {
		return State;
	}
	@FindBy(name="postalCode")
	private WebElement postalCode;
	public WebElement getpostalCode(){
		return postalCode;
	}
	@FindBy(name="country")
	private WebElement country;
	public WebElement getCountry() {
		return country;
	}
	@FindBy(id="email")
	private WebElement userName;
	public WebElement getuserName(){
		return userName;
	}
	@FindBy(name="password")
	private WebElement Password;
	public WebElement getPassword() {
		return Password;
	}
	@FindBy(name="confirmPassword")
	private WebElement ConfirmPassword;
	public WebElement getConfirmPassword() {
		return ConfirmPassword;
	}
	@FindBy(name="register")
	private WebElement Btn_submit;
	public WebElement getBtn_submit(){
		return Btn_submit;
	}
	
}