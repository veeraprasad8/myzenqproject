package testScripts;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.relevantcodes.extentreports.LogStatus;
import com.zenQ.constants.utilConstants;
import com.zenQ.driver.InitializerClass;

import functionLibrary.Utilities;
import functionLibrary.actions;
import pageObjects.FlightBooking_POM;


public class FlightBook extends InitializerClass {
	
	public FlightBook(WebDriver driver){
		util = new Utilities();
		flight = new FlightBooking_POM();
	}
	
	
	public String flight_Book(){
		
		try{
			Utilities.setExcelFile(utilConstants.pathDataSheet, 1);
			Utilities.loginApplication();
			waits.hardWait(4);
			actions.waitAndClick(flight.getType_RadioButton());
			extentTest.log(LogStatus.PASS, "Clicked on One Way Radio button");
			actions.selectValue(flight.getPassengers(), Utilities.getCellData(0, 1));
			extentTest.log(LogStatus.PASS, "Selected value: "+Utilities.getCellData(0, 1)+", in Passengers field");
			waits.hardWait(3);
			actions.selectValue(flight.getDeparture(), Utilities.getCellData(1, 1));
			extentTest.log(LogStatus.PASS, "Selected value: "+Utilities.getCellData(1, 1)+", in Departure field");
			waits.hardWait(3);
			actions.waitAndClick(flight.getContine_Button());
			extentTest.log(LogStatus.PASS, "Clicked on Continue Submit button");
			actions.waitAndClick(flight.getReserveContine_Button());
			extentTest.log(LogStatus.PASS, "Clicked on Reserve Continue Submit button");
			actions.waitAndClick(flight.getSecure_Purcharse());
			extentTest.log(LogStatus.PASS, "Clicked on Secure Purchase button for confirmation of booking");
			return "pass";
		}
		catch(Exception e){
			System.out.println(e);
			extentTest.log(LogStatus.ERROR, e);
			return "Fail";
		}catch(AssertionError e){
			System.out.println(e);
			extentTest.log(LogStatus.ERROR, e);
			return "Fail";
		}
		
	}

	
}
