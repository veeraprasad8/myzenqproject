package testScripts;

import org.openqa.selenium.WebDriver;

import com.relevantcodes.extentreports.LogStatus;
import com.zenQ.constants.utilConstants;
import com.zenQ.driver.InitializerClass;

import functionLibrary.Utilities;
import functionLibrary.actions;
import pageObjects.Register_POM;

public class Register extends InitializerClass{
	
	public Register(WebDriver driver){
		util = new Utilities();
		register = new Register_POM();
	}
	

	public String UserRegister() throws Exception{
		
				try{
					actions.waitAndClick(register.getLnk_Register());
					extentTest.log(LogStatus.PASS, "Clicked on Signup button");
				
					Utilities.setExcelFile(utilConstants.pathDataSheet, 0);
											
					actions.waitAndType(register.getFirstName(), Utilities.getCellData(0, 1));
					extentTest.log(LogStatus.PASS, "Text value: "+Utilities.getCellData(0, 1)+", entered in First name");
					
					actions.waitAndType(register.getLastName(), Utilities.getCellData(1, 1));
					extentTest.log(LogStatus.PASS, "Text value: "+Utilities.getCellData(1, 1)+", entered in Last name");
					
					actions.waitAndType(register.getphoneNumber(), Utilities.getCellData(2, 1));
					extentTest.log(LogStatus.PASS, "Text value: "+Utilities.getCellData(2, 1)+", entered in Phone number");
					
					actions.waitAndType(register.getEmail(), Utilities.getCellData(3, 1));
					extentTest.log(LogStatus.PASS, "Text value: "+Utilities.getCellData(3, 1)+", entered in Email");	
					
					actions.waitAndType(register.getAddress1(),Utilities.getCellData(4, 1));
					extentTest.log(LogStatus.PASS, "Text value: "+Utilities.getCellData(4, 1)+", entered in Address");		
					
					actions.waitAndType(register.getCity(), Utilities.getCellData(5, 1));
					extentTest.log(LogStatus.PASS, "Text value: "+Utilities.getCellData(5, 1)+", entered in City");
					
					actions.waitAndType(register.getState(), Utilities.getCellData(6, 1));
					extentTest.log(LogStatus.PASS, "Text value: "+Utilities.getCellData(6, 1)+", entered in State");
					
					actions.waitAndType(register.getpostalCode(), Utilities.getCellData(7, 1));
					extentTest.log(LogStatus.PASS, "Text value: "+Utilities.getCellData(7, 1)+", entered in Postal code");		
					
					actions.selectValue(register.getCountry(), Utilities.getCellData(8, 1));
					extentTest.log(LogStatus.PASS, "Selected value: "+Utilities.getCellData(8, 1)+", in Country field");
					waits.hardWait(2);
					
					actions.waitAndType(register.getuserName(), Utilities.getCellData(9, 1));					
					extentTest.log(LogStatus.PASS, "Text value: "+Utilities.getCellData(9, 1)+", entered in User name");		
					
				    actions.waitAndType(register.getPassword(), Utilities.getCellData(10, 1));  
					extentTest.log(LogStatus.PASS, "Text value: "+Utilities.getCellData(10, 1)+", entered in Password");
					
					actions.waitAndType(register.getConfirmPassword(), Utilities.getCellData(11, 1));
					extentTest.log(LogStatus.PASS, "Text value: "+Utilities.getCellData(11, 1)+", entered in Confirm password");
					
				    actions.waitAndClick(register.getBtn_submit());
					extentTest.log(LogStatus.PASS, "Clicked on Submit button");
								        
				    return "Pass";
					
				}catch(Exception e){
					System.out.println(e);
					extentTest.log(LogStatus.ERROR, e);
					return "Fail";
				}catch(AssertionError e){
					System.out.println(e);
					extentTest.log(LogStatus.ERROR, e);
					return "Fail";
				}
				

		}

}
